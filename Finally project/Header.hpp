#pragma once
#include <SFML/Graphics.hpp>
namespace A
{
    class Circle
    {
    public:
        Circle(int x, int y, float r, float speed)
        {
            m_x = x;
            m_y = y;
            m_r = r;
            m_speed = speed;
            m_shape = new sf::CircleShape(m_r);
            m_shape->setFillColor(sf::Color::White);
            m_shape->setPosition(m_x, m_y);


        }
        ~Circle()
        {
            delete m_shape;
        }

        sf::CircleShape* Get() { return m_shape; }

        void Move()
        {
            m_x = m_x - m_speed * 2;
            m_y = m_y + m_speed / 2;
            m_shape->setPosition(m_x, m_y);
        }
        void Spawn(int y)
        {
            m_x = 1200;   // �� ������
            m_y = y;
            m_shape->setPosition(m_x, m_y);
        }

        int Getx() { return m_x; }; //���������� ���������� �
        int Gety() { return m_y; }; //���������� ���������� y

    private:
        int m_x;
        int m_y;
        float m_r;
        float m_speed;
        sf::CircleShape* m_shape;
    };

    class Bear
    {
    public:
        Bear(int x, int y, float r, float speed)
        {
            m_x = x;
            m_y = y;
            m_r = r;
            m_speed = speed;
            m_shape = new sf::CircleShape(m_r);
            m_shape->setFillColor(sf::Color::White);
            m_shape->setPosition(m_x, m_y);
        }

        ~Bear()
        {
            delete m_shape;
        }

        sf::CircleShape* Get() { return m_shape; }

        void Move()
        {
            m_x = m_x - m_speed;
            m_shape->setPosition(m_x, m_y);
        }

    private:
        int m_x;
        int m_y;
        float m_r;
        float m_speed;
        sf::CircleShape* m_shape;
    };
}