#include <SFML/Graphics.hpp>
#include <iostream>
// ���������� ��� �������� ����
#include <chrono>
#include <thread>
#include <vector>
#include "Header.hpp"

using namespace std::chrono_literals;

int main()
{
    srand(time(0)); // ��������� ��������� �����

    // �������� ������� � ���������� �������� �����
    std::vector<A::Circle*> shapse;
    for (int i = -200; i <= 300; i += 60)
    {
        shapse.push_back(new A::Circle(1200, i, 2, rand() % 13 + 5));  // �������� ���������� �� ��������� 
    }

    // �������� ������� � ���������� ���������
    std::vector<A::Bear*> bear;
    bear.push_back(new A::Bear(1124, 120, 1, 0.15));
    bear.push_back(new A::Bear(1124, 140, 1, 0.15));
    bear.push_back(new A::Bear(1174, 120, 1, 0.15));
    bear.push_back(new A::Bear(1174, 140, 1, 0.15));
    bear.push_back(new A::Bear(1220, 150, 1, 0.15));
    bear.push_back(new A::Bear(1240, 165, 1, 0.15));
    bear.push_back(new A::Bear(1260, 190, 1, 0.15));



    //�������� � �������� �������� �� ����
    sf::Texture nebo;
    sf::Texture Moon;
    if ((!nebo.loadFromFile("img\\Sky.jpg")) || (!Moon.loadFromFile("img\\Moon.png")))
    {
        std::cout << "ERROR: File .jpg not found" << std::endl;
        return -1;
    }

    sf::Sprite background(nebo);   // Sprite ��� ��������� ������ ���������� ������������ � ��������� (�� ����� �������� Sky � �������� �� ������ ����� �������)
    sf::Sprite moon(Moon);

    // �������� ���� ������ ��������
    sf::RenderWindow window(sf::VideoMode(1024, 576), L"�������� ����");

    // ���������� ����
    float xM = 960;

    while (window.isOpen())
    {
        // �������� ������, ������� ��������� �������� ���� ��������� ��� ������� �� �������� ����
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // ��������� ���������� ����
        xM = xM - 0.9;
        moon.setPosition(xM, 5);

        // ��������� ��������� ��� �������� �����
        for (const auto& circle : shapse)
        {
            if ((circle->Getx() < -1000 || (circle->Gety() > 350)))
            {
                circle->Spawn(rand() % 300 - 200);
            }
            circle->Move();
        }

        // ��������� ��������� ��� ���������
        for (const auto& Bear : bear)
        {
            Bear->Move();
        }

        window.clear();

        // ��� (��� ������ ���������� ������)
        window.draw(background);

        //��������� ��������
        for (const auto& Bear : bear)
            window.draw(*Bear->Get());


        // ��������� �������� ������
        for (const auto& circle : shapse)
            window.draw(*circle->Get());

        // ��������� ����
        window.draw(moon);


        window.display();

        std::this_thread::sleep_for(40ms);

        // ���� ���� ����� �� ����� ������, �� ��������� ���������
        if (xM < 1)
        {
            for (const auto& circle : shapse)
                delete circle;
            shapse.clear();

            for (const auto& Bear : bear)
                delete Bear;
            bear.clear();

            std::cout << "end" << std::endl;
            return 1;
        }
    }

    return 0;
}





